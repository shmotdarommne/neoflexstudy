package ru.dbapp.demo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.dbapp.demo.db.Cars;
import ru.dbapp.demo.db.CarsRep;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class DBController {

    @Autowired
    private CarsRep carsRep;

    @GetMapping
    public Cars showStatus() {
        return new Cars();
    }


    @GetMapping("/Cars/{id}")
    public Optional<Cars> createCars(@PathVariable Long id) {
        return carsRep.findById(id);
    }

    @PostMapping("/Cars/new")
    public  List<Cars>   createCars(@RequestBody List<Cars> listCars) {
        for (Cars car : listCars) {
            carsRep.save(car);
        }
        return listCars ;
    }
}
